package ic.app


import ic.apps.ic.ui.cmd.IcCommand
import ic.base.annotations.UsedByReflect
import ic.base.kfunctions.ext.getValue
import ic.struct.value.cached.Cached


@UsedByReflect
class Main : CommandLineApp() {

	override val command by Cached {
		IcCommand()
	}

}