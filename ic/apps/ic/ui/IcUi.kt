package ic.apps.ic.ui


import ic.text.Text
import ic.util.auth.BasicAuth
import ic.util.auth.UrlAndAuth


interface IcUi {

	fun onPackageAddStarted (packageName: String)
	fun onPackageUpdateStarted (packageName: String)
	fun notifyPackageBuildStarted (packageName: String)
	fun onPackageUploadStarted (packageName: String)
	fun onPackageTestStarted (packageName: String)
	fun onPackageTestPassed (packageName: String, testResult: Text)
	fun onPackageTestFailed (packageName: String, testResult: Text)
	fun onSfxPackingStarted()

	fun notifyLazyLangImportStarted()
	fun notifyLazyLangImportSuccess()
	fun notifyLazyLangCompileStarted()
	fun notifyLazyLangCompileSuccess()

	fun handleWarning (warning: String)
	fun handleWarning (warning: Text)

	fun askPullAuth (urlAndAuth: UrlAndAuth) : BasicAuth<String?, String?>
	fun askPushAuth (urlAndAuth: UrlAndAuth) : BasicAuth<String?, String?>

}
