package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.NotExistsException

import ic.cmd.Command
import ic.cmd.Console

import ic.apps.ic.funs.versions.branch
import ic.apps.ic.ui.cmd.ConsoleIcUi
import ic.base.strings.ext.asText


abstract class BranchCommand : Command() {


	override val name get() = "branch"

	override val syntax get() = "branch <package_name> <branch_name>"

	override val description get() = "Create new branch"


	protected abstract val distribution : Distribution


	@Throws(InvalidSyntaxException::class)
	override fun implementRun (console: Console, args: String) {

		val argsIterator = args.asText.newIterator()

		val packageName = try {
			argsIterator.safeReadTill(' ').toString()
		} catch (notExists: NotExistsException) { throw InvalidSyntaxException }

		val branchName = argsIterator.readTill(' ').toString()

		distribution.branch(
			packageName = packageName,
			branchName = branchName,
			icUi = ConsoleIcUi(console)
		)

	}


}