package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.getPackageOrThrow
import ic.apps.ic.dist.ext.save
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.NotExistsException
import ic.cmd.Command
import ic.cmd.Console

import ic.base.strings.ext.asText
import ic.base.throwables.MessageException
import ic.app.res.funs.getResString


abstract class SetStoreCommand : Command() {


	override val name get() = "set-store"

	override val syntax get() = "set-store <package_name> <store_name>"

	override val description get() = "Set origin store for package"


	protected abstract val distribution : Distribution


	@Throws(MessageException::class)
	override fun implementRun (console: Console, args: String) {
		val argsIterator = args.asText.newIterator()
		val packageName = try {
			argsIterator.safeReadTill(' ').toString()
		} catch (notExists: NotExistsException) { throw InvalidSyntaxException }
		val storeName = argsIterator.read().toString()
		val distribution = distribution
		val pkg = try {
			distribution.getPackageOrThrow(packageName)
		} catch (notExists: NotExistsException) {
			throw MessageException(
				getResString("ic/package-not-exists", "$(packageName)" to packageName)
			)
		}
		pkg.storeName = storeName
		distribution.save()
	}


}