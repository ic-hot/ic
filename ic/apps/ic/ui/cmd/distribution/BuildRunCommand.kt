package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.struct.set.finite.FiniteSet
import ic.system.funs.launchBashCommand

import ic.cmd.Command
import ic.cmd.Console

import ic.apps.ic.dist.ext.launchDirectory
import ic.apps.ic.funs.build.build
import ic.apps.ic.ui.cmd.ConsoleIcUi
import ic.storage.fs.FsLocation


abstract class BuildRunCommand : Command() {


	override val name 			get() = "buildAndRun"
	override val syntax 		get() = "buildAndRun <app_package_name>"
	override val description 	get() = "Compile and launch app"


	protected abstract val distribution : Distribution


	override fun implementRun (console: Console, args: String) {

		val distribution = distribution

		val appPackageName = args

		distribution.build(
			packageNamesToBuild = FiniteSet(appPackageName),
			icUi = ConsoleIcUi(console, silent = true)
		)

		console.writeLine(
			"Compiled successfully. Now starting..."
		)

		launchBashCommand(
			FsLocation(distribution.launchDirectory, appPackageName).absolutePath
		)

	}


}