package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.apps.ic.funs.remove.removePackageOrThrow
import ic.base.strings.ext.isEmpty
import ic.cmd.Command
import ic.cmd.Console
import ic.base.throwables.NotExistsException


internal abstract class RemoveCommand : Command() {


	override val name get() = "remove"

	override val syntax get() = "remove <package_name>"

	override val description get() = "Remove package from project"


	protected abstract val distribution : Distribution


	override fun implementRun (console: Console, args: String) {
		val packageName = args
		if (packageName.isEmpty) {
			console.writeLine("Package name can't be empty")
			return
		}
		try {
			distribution.removePackageOrThrow(packageName)
		} catch (notExists: NotExistsException) {
			console.writeLine("Package $packageName not found")
		}
	}

}