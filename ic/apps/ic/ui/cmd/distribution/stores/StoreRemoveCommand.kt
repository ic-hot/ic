package ic.apps.ic.ui.cmd.distribution.stores


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.removeStoreOrThrow
import ic.cmd.Command
import ic.cmd.Console
import ic.base.strings.ext.asText
import ic.base.throwables.NotExistsException
import ic.base.throwables.MessageException


abstract class StoreRemoveCommand : Command() {


	override val name get() = "remove"

	override val syntax get() = "remove <store_name>"

	override val description get() = "Remove store"


	protected abstract val distribution : Distribution


	@Throws(MessageException::class)
	override fun implementRun(console: Console, args: String) {
		val argsIterator = args.asText.newIterator()
		val storeName = argsIterator.read().toString()
		try {
			distribution.removeStoreOrThrow(storeName)
		} catch (_: NotExistsException) {
			throw MessageException("No such store")
		}
	}


}