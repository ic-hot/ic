package ic.apps.ic.ui.cmd.distribution.stores


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.addStoreOrThrow
import ic.apps.ic.dist.ext.save
import ic.cmd.Command
import ic.cmd.Console
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.NotExistsException

import ic.base.strings.ext.asText
import ic.base.throwables.MessageException


abstract class AddStoreCommand : Command() {


	override val name get() = "add"

	override val syntax get() = "add <store_name> <store_url>"

	override val description get() = "Add new store"


	protected abstract val distribution: Distribution


	@Throws(MessageException::class, InvalidSyntaxException::class)
	override fun implementRun(console: Console, args: String) {
		val argsIterator = args.asText.newIterator()
		val storeName = try {
			argsIterator.safeReadTill(' ')
		} catch (notExists: NotExistsException) { throw InvalidSyntaxException }
		if (storeName.isEmpty) throw InvalidSyntaxException
		val rawUrl = argsIterator.read()
		if (rawUrl.isEmpty) throw InvalidSyntaxException
		try {
			distribution.addStoreOrThrow(
				name = storeName.toString(),
				url = rawUrl.toString()
			)
			distribution.save()
		} catch (_: AlreadyExistsException) {
			throw MessageException("Store $storeName already exists")
		}
	}


}