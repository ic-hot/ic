package ic.apps.ic.ui.cmd.distribution.stores


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.getStoreOrThrow
import ic.cmd.Command
import ic.cmd.Console
import ic.base.strings.ext.asText
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException
import ic.base.throwables.NotExistsException


abstract class StoreSetUrlCommand : Command() {


	override val name get() = "set-url"

	override val syntax get() = "set-url <store_name> <store_url>"

	override val description get() = "Change store's url"


	protected abstract val distribution : Distribution


	@Throws(MessageException::class, InvalidSyntaxException::class)
	override fun implementRun(console: Console, args: String) {
		val argsIterator = args.asText.newIterator()
		val storeName = try {
			argsIterator.safeReadTill(' ')
		} catch (notExists: NotExistsException) { throw InvalidSyntaxException }
		if (storeName.isEmpty) throw InvalidSyntaxException
		val url = argsIterator.read()
		if (url.isEmpty) throw InvalidSyntaxException
		val distribution = distribution
		val store = try {
			distribution.getStoreOrThrow(storeName.toString())
		} catch (notExists: NotExistsException) {
			throw MessageException("Store $storeName not exists")
		}
		store.url = url.toString()
	}


}