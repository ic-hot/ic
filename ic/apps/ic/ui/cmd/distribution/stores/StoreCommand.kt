package ic.apps.ic.ui.cmd.distribution.stores


import ic.apps.ic.dist.Distribution
import ic.base.kfunctions.ext.getValue
import ic.cmd.SelectorCommand
import ic.struct.list.List
import ic.struct.value.cached.Cached


abstract class StoreCommand : SelectorCommand() {


	override val name get() = "store"

	override val syntax get() = "store"

	override val description get() = "Stores shell"

	override val shellWelcome get() = null

	override val shellTitle get() = "Stores shell:"


	protected abstract val distribution : Distribution


	override val children by Cached {
		List(
			object : AddStoreCommand() {
				override val distribution get() = this@StoreCommand.distribution
			},
			object : StoreRemoveCommand() {
				override val distribution get() = this@StoreCommand.distribution
			},
			object : StoreSetUrlCommand() {
				override val distribution get() = this@StoreCommand.distribution
			}
		)
	}


}