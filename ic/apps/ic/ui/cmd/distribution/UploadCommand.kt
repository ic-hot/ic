package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.base.strings.ext.isEmpty
import ic.base.strings.ext.split
import ic.struct.list.ext.copy.copyToFiniteSet
import ic.cmd.Command
import ic.cmd.Console

import ic.apps.ic.dist.ext.packageNames
import ic.apps.ic.funs.upload.upload
import ic.apps.ic.ui.cmd.ConsoleIcUi


abstract class UploadCommand : Command() {


	override val name get() = "upload"

	override val syntax get() = "upload [<package_names>]"

	override val description get() = "Upload changes to store"


	protected abstract val distribution : Distribution

	override fun implementRun (console: Console, args: String) {
		val distribution : Distribution = distribution
		val packageNames = (
			if (args.isEmpty) {
				distribution.packageNames
			} else {
				args.split(separator = ' ').copyToFiniteSet()
			}
		)
		console.writeLine("Enter commit message:")
		val commitMessage = console.readLineAsText().toString()
		distribution.upload(
			packageNames = packageNames,
			commitMessage = commitMessage,
			icUi = ConsoleIcUi(console)
		)
	}


}