package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.getPackage
import ic.apps.ic.dist.ext.packageNames
import ic.cmd.Command
import ic.cmd.Console
import ic.cmd.writeTable
import ic.struct.collection.ext.copy.copySortToList
import ic.struct.list.List
import ic.struct.list.ext.copy.convert.copyConvert


abstract class ListCommand : Command() {


	override val name get() = "list"

	override val syntax get() = "list"

	override val description get() = "List packages"


	protected abstract val distribution : Distribution


	override fun implementRun (console: Console, args: String) {
		val distribution: Distribution = distribution
		writeTable(
			console,
			distribution.packageNames.copySortToList().copyConvert {
				packageName -> List(packageName, distribution.getPackage(packageName).storeName)
			},
			1
		)
	}

}