package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.packageNames
import ic.apps.ic.funs.update.update
import ic.apps.ic.ui.cmd.ConsoleIcUi
import ic.base.strings.ext.isEmpty

import ic.cmd.Command
import ic.cmd.Console
import ic.struct.sequence.ext.copy.copyConvertToFiniteSet
import ic.text.SplitText

abstract class UpdateCommand : Command() {


	override val name get() = "update"

	override val syntax get() = "update [<package_names>]"

	override val description get() = "Download changes from store"


	protected abstract val distribution : Distribution


	override fun implementRun(console: Console, args: String) {
		val distribution = distribution
		val packagesToUpdate = (
			if (args.isEmpty) {
				distribution.packageNames
			} else {
				SplitText(args, ' ').copyConvertToFiniteSet { it.toString() }
			}
		)
		distribution.update(
			packagesToUpdate = packagesToUpdate,
			icUi = ConsoleIcUi(console)
		)
	}
}