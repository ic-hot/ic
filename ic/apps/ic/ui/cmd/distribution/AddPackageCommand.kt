package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException

import ic.cmd.Command
import ic.cmd.Console

import ic.apps.ic.funs.add.addPackage
import ic.apps.ic.ui.cmd.ConsoleIcUi
import ic.base.strings.ext.isEmpty


internal abstract class AddPackageCommand : Command() {


	override val name 			get() = "add"
	override val syntax 		get() = "add <package_name>"
	override val description 	get() = "Add package to project"


	protected abstract val distribution : Distribution


	override fun implementRun (console: Console, args: String) {
		val packageName = args
		if (packageName.isEmpty) {
			console.writeLine("Package name can't be empty")
			return
		}
		try {
			distribution.addPackage(
				packageName,
				ConsoleIcUi(console)
			)
		} catch (alreadyExists: AlreadyExistsException) {
			console.writeLine("Package $packageName already added")
		} catch (notExists: NotExistsException) {
			console.writeLine("Package $packageName not found")
		}
	}


}