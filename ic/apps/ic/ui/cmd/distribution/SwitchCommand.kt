package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.cmd.Command
import ic.cmd.Console
import ic.text.input.TextInput
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.NotExistsException
import ic.base.throwables.NotNeededException

import ic.apps.ic.funs.throwables.PackageNotExists
import ic.apps.ic.funs.versions.switchVersion
import ic.apps.ic.ui.cmd.ConsoleIcUi
import ic.base.strings.ext.asText


abstract class SwitchCommand : Command() {


	override val name get() = "switch"

	override val syntax get() = "switch <package_name> <version>"

	override val description get() = "Checkout to branch or commit"


	protected abstract val distribution : Distribution


	override fun implementRun(console: Console, args: String) {
		val argsIterator: TextInput = args.asText.newIterator()
		val packageName: String
		packageName = try {
			argsIterator.safeReadTill(' ').toString()
		} catch (notExists: NotExistsException) { throw InvalidSyntaxException }
		val version = argsIterator.read(' '.code).toString()
		try {
			distribution.switchVersion(
				packageName = packageName,
				version = version,
				icUi = ConsoleIcUi(console)
			)
		} catch (packageNotExists: PackageNotExists) {
			console.writeLine("Package ${ packageNotExists.packageName } not exists")
		} catch (notExists: NotExistsException) {
			console.writeLine("Version $version not exists")
		} catch (notNeeded: NotNeededException) {
			console.writeLine("Already on $version")
		}
	}


}