package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.struct.list.List

import ic.cmd.SelectorCommand

import ic.apps.ic.ui.cmd.distribution.stores.StoreCommand
import ic.base.escape.skip.skip
import ic.base.kfunctions.ext.getValue
import ic.struct.value.cached.Cached


abstract class DistributionSelectorCommand : SelectorCommand() {

	protected abstract val distribution : Distribution

	override val children by Cached {
		List(
			{
				object : StoreCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				object : ListCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				object : AddPackageCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				object : RemoveCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				object : UpdateCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				object : SetStoreCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				if (distribution.toBuildOnUpdate) skip
				object : BuildCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				if (distribution.toBuildOnUpdate) skip
				object : BuildRunCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}, {
				object : UploadCommand() {
					override val distribution get() = this@DistributionSelectorCommand.distribution
				}
			}
		)
	}

}