package ic.apps.ic.ui.cmd.distribution


import ic.apps.ic.dist.Distribution
import ic.cmd.Command
import ic.cmd.Console

import ic.apps.ic.funs.build.build
import ic.apps.ic.dist.ext.packageNames
import ic.apps.ic.ui.cmd.ConsoleIcUi
import ic.base.strings.ext.asText
import ic.base.strings.ext.split
import ic.struct.list.ext.copy.copyToFiniteSet


abstract class BuildCommand : Command() {


	override val name 			get() = "build"
	override val syntax 		get() = "build [<package_names>]"
	override val description 	get() = "Compile and test packages"


	protected abstract val distribution : Distribution


	override fun implementRun (console: Console, args: String) {

		val distribution = distribution

		val packageNamesToBuild = if (args.asText.isEmpty) {
			distribution.packageNames
		} else {
			args.split(separator = ' ').copyToFiniteSet()
		}

		distribution.build(
			packageNamesToBuild,
			ConsoleIcUi(console)
		)

	}


}