package ic.apps.ic.ui.cmd


import ic.app.res.funs.getResString
import ic.text.Text
import ic.util.auth.BasicAuth
import ic.util.auth.UrlAndAuth

import ic.cmd.Console

import ic.apps.ic.ui.IcUi


class ConsoleIcUi (
	private val console : Console,
	private val silent : Boolean = false
) : IcUi {

	override fun onPackageAddStarted (packageName: String) = console.writeLine("Package adding: $packageName ...")

	override fun onPackageUpdateStarted (packageName: String) = console.writeLine("Package updating: $packageName ...")

	override fun notifyPackageBuildStarted (packageName: String) {
		if (silent) return
		console.writeLine("Package building: $packageName ...")
	}

	override fun onPackageUploadStarted (packageName: String) = console.writeLine("Package uploading: $packageName ...")

	override fun onPackageTestStarted (packageName: String) = console.writeLine("Package testing: $packageName ...")

	override fun onPackageTestPassed (packageName: String, testResult: Text) { console.writeLine(testResult) }
	override fun onPackageTestFailed (packageName: String, testResult: Text) { console.writeLine(testResult) }

	override fun onSfxPackingStarted() = console.writeLine("Packing to self extracting archive...")

	override fun notifyLazyLangImportStarted()  = console.writeLine(getResString("ic.LazyLangImportStarted"))
	override fun notifyLazyLangImportSuccess()  = console.writeLine(getResString("ic.LazyLangImportSuccess"))
	override fun notifyLazyLangCompileStarted() = console.writeLine(getResString("ic.LazyLangCompileStarted"))
	override fun notifyLazyLangCompileSuccess() = console.writeLine(getResString("ic.LazyLangCompileSuccess"))

	override fun handleWarning (warning: String) {
		if (warning.startsWith("warning: redirecting to")) return
		console.writeLine(warning)
	}

	override fun handleWarning (warning: Text) = handleWarning(warning.toString())

	override fun askPullAuth (urlAndAuth: UrlAndAuth): BasicAuth<String?, String?> {
		val username = if (urlAndAuth.auth.username == null) {
			console.writeLine("Enter username for pulling from ${ urlAndAuth.url }")
			console.writeLine("(or leave empty if username not needed):")
			val userNameFromConsole = console.readLineAsText()
			if (userNameFromConsole.isEmpty) {
				null
			} else {
				userNameFromConsole.toString()
			}
		} else {
			urlAndAuth.auth.username
		}
		val password = if (urlAndAuth.auth.password == null) {
			console.writeLine(
				if (username == null) {
					"Enter password for pulling from ${ urlAndAuth.url }"
				} else {
					"Enter password for $username on ${ urlAndAuth.url }"
				}
			)
			console.writeLine("(or leave empty if password not needed):")
			val passwordFromConsole = console.readLineAsText()
			if (passwordFromConsole.isEmpty) {
				null
			} else {
				passwordFromConsole.toString()
			}
		} else {
			urlAndAuth.auth.password
		}
		return BasicAuth(username, password)
	}

	override fun askPushAuth (urlAndAuth: UrlAndAuth) : BasicAuth<String?, String?> {
		val username = if (urlAndAuth.auth.username == null) {
			console.writeLine("Enter username for pushing to ${ urlAndAuth.url }")
			console.writeLine("(or leave empty if username not needed):")
			val userNameFromConsole = console.readLineAsText()
			if (userNameFromConsole.isEmpty) {
				null
			} else {
				userNameFromConsole.toString()
			}
		} else {
			urlAndAuth.auth.username
		}
		val password = if (urlAndAuth.auth.password == null) {
			console.writeLine(
				if (username == null) {
					"Enter password for pushing to ${ urlAndAuth.url }"
				} else {
					"Enter password for $username on ${ urlAndAuth.url }"
				}
			)
			console.writeLine("(or leave empty if password not needed):")
			val passwordFromConsole = console.readLineAsText()
			if (passwordFromConsole.isEmpty) {
				null
			} else {
				passwordFromConsole.toString()
			}
		} else {
			urlAndAuth.auth.password
		}
		return BasicAuth(username, password)
	}

}