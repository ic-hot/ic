package ic.apps.ic.ui.cmd


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ProjectType
import ic.apps.ic.dist.ext.createPortable
import ic.base.throwables.InvalidSyntaxException
import ic.base.strings.ext.asText
import ic.storage.fs.local.ext.create
import ic.storage.fs.local.workdir
import ic.text.input.TextInput

import ic.cmd.Command
import ic.cmd.Console

import ic.storage.fs.Directory


class NewDistributionCommand : Command() {


	override val name get() = "new"

	override val syntax get() = "new noproject|default|gradle <path>"

	override val description get() = "Create new distribution"


	@Throws(InvalidSyntaxException::class, RuntimeException::class)
	override fun implementRun(console: Console, args: String) {
		val argsIterator: TextInput = args.asText.newIterator()
		val projectTypeString = argsIterator.readTill(' ').toString()
		val path = argsIterator.readTill(' ').toString()
		val projectType = when (projectTypeString) {
			"noproject" -> null
			"default" -> ProjectType.Default
			"gradle" -> ProjectType.Gradle
			else -> throw InvalidSyntaxException
		}
		val rootDirectory = Directory.create(workdir, path)
		Distribution.createPortable(rootDirectory, projectType)
	}


}