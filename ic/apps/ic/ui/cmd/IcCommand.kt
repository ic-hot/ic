package ic.apps.ic.ui.cmd


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.global
import ic.apps.ic.dist.ext.local
import ic.cmd.SelectorCommand
import ic.app.res.funs.getResString
import ic.struct.list.List

import ic.apps.ic.ui.cmd.distribution.DistributionSelectorCommand
import ic.base.kfunctions.ext.getValue
import ic.cmd.Command
import ic.struct.value.cached.Cached


class IcCommand : SelectorCommand() {


	override val name get() = "ic"

	override val syntax get() = "ic"

	override val description get() = getResString("ic/ic-command-description")

	override val shellWelcome get() = null

	override val shellTitle get() = getResString("ic/ic-shell-title")


	override val children by Cached {
		List(
			object : DistributionSelectorCommand() {
				override val distribution get() = Distribution.local
				override val name get() = "local"
				override val syntax get() = "local"
				override val description get() = "Distribution in current work directory"
				override val shellWelcome get() = null
				override val shellTitle get() = "Local distribution:"
			},
			object : DistributionSelectorCommand() {
				override val distribution get() = Distribution.global
				override val name get() = "global"
				override val syntax get() = "global"
				override val description get() = "System-wide distribution"
				override val shellWelcome get() = null
				override val shellTitle get() = "Global distribution:"
			},
			NewDistributionCommand()
		)
	}

}