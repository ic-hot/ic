package ic.apps.ic.funs.update


import ic.base.strings.ext.find.findLastIndexOf
import ic.base.strings.ext.getSubstring
import ic.base.throwables.*
import ic.storage.fs.ext.writeFileIfNotExists
import ic.app.res.funs.getResString
import ic.struct.collection.ext.foreach.forEach
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.list.ext.foreach.forEach
import ic.network.http.sendHttpRequest
import ic.util.auth.getAuth
import ic.util.log.LogLevel

import ic.git.Conflict
import ic.git.gitPullCommitingIfNeeded

import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.Store
import ic.apps.ic.dist.ext.*
import ic.apps.ic.dist.ext.getPackageJarDirectory
import ic.apps.ic.dist.ext.getPackageSrcRootDirOrThrow
import ic.apps.ic.funs.add.implementAddPackage
import ic.apps.ic.funs.add.implementClonePackage
import ic.apps.ic.ui.IcUi


internal fun Distribution.updatePackage (

	session : UpdateSession,

	packageName : String,

	icUi : IcUi

) {

	if (session.updatedPackages.contains(packageName)) return

	if (!session.alreadyDownloadedPackages.contains(packageName)) {

		try { val packageSrcRootDir = getPackageSrcRootDirOrThrow(packageName)

			icUi.onPackageUpdateStarted(packageName)

			val store = getStore(getPackage(packageName).storeName)

			val remoteAuth = getAuth(store.url, "read") { icUi.askPullAuth(it) }

			val repositoryUrl = store.url + "/" + packageName
			try {
				gitPullCommitingIfNeeded(
					repositoryDir = packageSrcRootDir,
					url = repositoryUrl,
					auth = remoteAuth,
					handleWarning = { icUi.handleWarning(it) }
				)
			} catch (notExists: NotExistsException) {
				throw MessageException(
					getResString("ic/repository-not-exists", "$(url)" to repositoryUrl)
				)
			} catch (conflict: Conflict) {
				icUi.handleWarning(conflict.message)
			} catch (notNeeded: NotNeededException) {}

		} catch (packageSrcRootDirNotExists: NotExistsException) {

			if (isPackageExists(packageName)) {

				icUi.onPackageUpdateStarted(packageName)

				val store = getStore(getPackage(packageName).storeName)

				try {
					implementClonePackage(packageName, store, icUi)
				} catch (notExists: NotExistsException) {
					throw MessageException("Can't find package " + packageName + " in " + store.name)
				}

			} else {

				try {
					implementAddPackage(
						packageName = packageName,
						icUi 		= icUi
					)
				} catch (alreadyExists: AlreadyExistsException) {
					throw InconsistentDataException(alreadyExists)
				} catch (notExists: NotExistsException) 			{
					throw MessageException("Can't find package $packageName in stores");
				}

			}

		}

	}

	session.updatedPackages.add(packageName)

	val pkgDecl = getPackageDeclaration(packageName)

	pkgDecl.stores.forEach { store ->
		if (!isStoreExists(store.name)) {
			storesAsEditableList.add(store)
		}
	}
	save()

	val packageJarDirectory = getPackageJarDirectory(packageName)
	pkgDecl.jarDependencies.forEach { jarDependency ->
		val fileContent = sendHttpRequest(
			urlString = jarDependency,
			successLogLevel = LogLevel.None
		).body
		val fileName = jarDependency.getSubstring(
			startIndex = jarDependency.findLastIndexOf('/') + 1,
			endIndex = jarDependency.length
		)
		packageJarDirectory.writeFileIfNotExists(fileName, fileContent)
	}

	pkgDecl.dependencies.forEach { dependencyPackageName ->

		if (isPackageExists(dependencyPackageName)) return@forEach

		updatePackage(
			session 	= session,
			packageName = dependencyPackageName,
			icUi 		= icUi
		)

	}

}