package ic.apps.ic.funs.update.project.default


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.getPackage
import ic.struct.collection.filter.FilterCollection
import ic.system.localcmd.LocalBashSession

import ic.apps.ic.dist.ext.packageNames
import ic.apps.ic.dist.ext.stores
import ic.storage.fs.ext.createFolder
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.local.ext.createSymbolicLink
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.ext.foreach.forEach


internal fun Distribution.updateDefaultProject() {

	val packageNames = packageNames

	val tmpModulesDirectory = directory.createFolder(".tmp.packages")

	val bashSession = LocalBashSession(directory)

	try {

		stores.forEach { store ->

			val storeDirectory = tmpModulesDirectory.createFolderIfNotExists(store.name)

			storeDirectory.empty()

			FilterCollection(packageNames) { getPackage(it).storeName == store.name }.forEach { packageName ->

				storeDirectory.createSymbolicLink(packageName, "../../.ic/src/$packageName")

			}

		}

		bashSession.executeCommand("rm -rf packages; mv .tmp.packages packages")

	} catch (any: Throwable) {

		bashSession.executeCommand("rm -rf .tmp.packages")

		throw any

	}

}