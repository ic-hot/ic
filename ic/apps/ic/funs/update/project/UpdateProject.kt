package ic.apps.ic.funs.update.project


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ProjectType
import ic.apps.ic.funs.update.project.default.updateDefaultProject
import ic.apps.ic.funs.update.project.gradle.updateGradleProject


internal fun Distribution.updateProject() = when (projectType) {
	null -> {}
	ProjectType.Default -> updateDefaultProject()
	ProjectType.Gradle  -> updateGradleProject()
}
