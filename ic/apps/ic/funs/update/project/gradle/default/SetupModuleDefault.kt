package ic.apps.ic.funs.update.project.gradle.default


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.PackageDeclaration
import ic.apps.ic.funs.update.project.gradle.impl.formatDependencies
import ic.apps.ic.kotlinVersion
import ic.storage.fs.Directory
import ic.storage.fs.local.ext.createSymbolicLink
import ic.storage.fs.local.ext.createSymbolicLinkIfExists
import ic.storage.fs.ext.writeFileReplacing
import ic.struct.list.List
import ic.struct.list.ext.concat
import ic.text.TextBuffer
import ic.text.ext.toByteSequence
import ic.util.text.charset.Charset


internal fun Distribution.setupModuleDefault(

	packageName : String,
	packageDeclaration : PackageDeclaration,

	moduleDirectory : Directory,
	mainDirectory: Directory

) {

	mainDirectory.createSymbolicLink("src", "../../../../.ic/src/$packageName")

	mainDirectory.createSymbolicLinkIfExists("res", "../../../../.ic/src/$packageName/res")

	val text = TextBuffer()

	text.writeLine(
		List(
			"plugins {",
			"	id 'java'",
			"	id 'org.jetbrains.kotlin.jvm' version '$kotlinVersion'",
			"}",
			"",
			"sourceSets {",
			"	main {",
			"		java {",
			"			srcDirs = ['main/src', 'main/gen']",
			"		}",
			"		resources {",
			"			srcDirs = ['main/res']",
			"		}",
			"	}",
			"}",
			"",
			"repositories {",
			"	mavenCentral()",
			"}"
		).concat(separator = '\n')
	)

	text.writeNewLine()

	formatDependencies(
		packageDeclaration = packageDeclaration,
		output = text
	)

	text.writeNewLine()

	text.writeLine(
		List(
			"compileKotlin {",
			"	kotlinOptions {",
			"		jvmTarget = '11'",
			"		freeCompilerArgs = ['-Xjvm-default=all']",
			"	}",
			"}",
			"",
			"compileTestKotlin {",
			"	kotlinOptions {",
			"		jvmTarget = '11'",
			"		freeCompilerArgs = ['-Xjvm-default=all']",
			"	}",
			"}"
		).concat(separator = '\n')
	)

	moduleDirectory.writeFileReplacing("build.gradle", text.toByteSequence(Charset.defaultUnix))

}