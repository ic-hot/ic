package ic.apps.ic.funs.update.project.gradle.impl


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.PackageDeclaration
import ic.apps.ic.dist.ext.getPackage
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.List
import ic.struct.list.ext.concat
import ic.text.TextOutput


internal fun Distribution.formatDependencies (

	packageDeclaration : PackageDeclaration,

	output :  TextOutput

) {

	output.writeLine(
		List(
			"dependencies {",
			"	implementation fileTree(dir: 'jar', include: ['*.jar'])",
			"	implementation fileTree(dir: 'main/jar', include: ['*.jar'])"
		).concat(separator = '\n')
	)

	packageDeclaration.dependencies.forEach { dependencyPackageName ->

		val storeName = getPackage(dependencyPackageName).storeName

		output.writeLine(
			"	implementation project(':packages:$storeName:$dependencyPackageName')"
		)

	}

	output.writeLine("}")

}