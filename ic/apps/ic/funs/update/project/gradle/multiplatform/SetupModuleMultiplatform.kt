package ic.apps.ic.funs.update.project.gradle.multiplatform


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.PackageDeclaration
import ic.apps.ic.dist.ext.multiplatformDirsNames
import ic.apps.ic.funs.update.project.gradle.impl.formatDependencies
import ic.apps.ic.kotlinVersion
import ic.storage.fs.Directory
import ic.storage.fs.ext.createFolder
import ic.storage.fs.local.ext.createSymbolicLinkIfExists
import ic.storage.fs.ext.writeFileReplacing
import ic.struct.list.List
import ic.struct.list.ext.concat
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.foreach.breakableForEach
import ic.struct.list.ext.foreach.forEach
import ic.text.TextBuffer
import ic.text.ext.toByteSequence
import ic.util.text.charset.Charset


internal fun Distribution.setupModuleMultiplatform (

	packageName : String,
	packageDeclaration : PackageDeclaration,

	moduleDirectory : Directory,
	mainDirectory   : Directory,

) {

	val srcDirectory = mainDirectory.createFolder("src")
	multiplatformDirsNames.forEach { multiplatformDirName ->
		srcDirectory.createSymbolicLinkIfExists(
			name = multiplatformDirName,
			linkPath = "../../../../../.ic/src/$packageName/$multiplatformDirName"
		)
	}

	val resDirectory = mainDirectory.createFolder("res")
	multiplatformDirsNames.forEach { multiplatformDirName ->
		resDirectory.createSymbolicLinkIfExists(
			name = multiplatformDirName,
			linkPath = "../../../../../.ic/src/$packageName/$multiplatformDirName/res"
		)
	}

	val text = TextBuffer()

	val srcDirsGroovy = (
		"[" +
			multiplatformDirsNames.copyConvert { multiplatformDirName ->
			"'main/src/$multiplatformDirName'"
		}.concat(separator = ", ") +
		"]"
	)

	val resDirsGroovy = (
		"[" +
			multiplatformDirsNames.copyConvert { multiplatformDirName ->
			"'main/res/$multiplatformDirName'"
		}.concat(separator = ", ") +
		"]"
	)

	text.writeLine(
		List(
			"plugins {",
			"	id 'java'",
			"	id 'org.jetbrains.kotlin.jvm' version '$kotlinVersion'",
			"}",
			"",
			"sourceSets {",
			"	main {",
			"		java {",
			"			srcDirs = $srcDirsGroovy",
			"		}",
			"		resources {",
			"			srcDirs = $resDirsGroovy",
			"		}",
			"	}",
			"}",
			"",
			"repositories {",
			"	mavenCentral()",
			"}"
		).concat(separator = '\n')
	)

	text.writeNewLine()

	formatDependencies(
		packageDeclaration = packageDeclaration,
		output = text
	)

	text.writeNewLine()

	text.writeLine(
		List(
			"compileKotlin {",
			"	kotlinOptions {",
			"		jvmTarget = '11'",
			"		freeCompilerArgs = ['-Xjvm-default=all']",
			"	}",
			"}",
			"",
			"compileTestKotlin {",
			"	kotlinOptions {",
			"		jvmTarget = '11'",
			"		freeCompilerArgs = ['-Xjvm-default=all']",
			"	}",
			"}"
		).concat(separator = '\n')
	)

	moduleDirectory.writeFileReplacing("build.gradle", text.toByteSequence(Charset.defaultUnix))

}