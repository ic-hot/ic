package ic.apps.ic.funs.update.project.gradle


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.PackageDeclaration
import ic.apps.ic.dist.ext.getPackage
import ic.apps.ic.dist.ext.getPackageDeclaration
import ic.system.localcmd.LocalBashSession
import ic.util.text.charset.Charset
import ic.base.strings.ext.toByteSequence

import ic.apps.ic.dist.ext.packageNames
import ic.apps.ic.dist.ext.stores
import ic.apps.ic.funs.update.project.gradle.default.setupModuleDefault
import ic.apps.ic.funs.update.project.gradle.multiplatform.setupModuleMultiplatform
import ic.storage.fs.ext.*
import ic.storage.fs.local.ext.createSymbolicLink
import ic.storage.fs.local.ext.createSymbolicLinkIfExists
import ic.struct.collection.ext.copy.copyConvertToList
import ic.struct.collection.ext.copy.copyFilter
import ic.struct.list.ext.concat
import ic.struct.list.ext.foreach.forEach


internal fun Distribution.updateGradleProject() {

	val packageNames = packageNames

	directory.writeFileReplacing("build.gradle", "".toByteSequence(Charset.defaultUnix))

	directory.writeFileReplacing(
		"settings.gradle",
		packageNames
			.copyConvertToList { packageName ->
				"include ':packages:${ getPackage(packageName).storeName }:$packageName'"
			}
			.concat(separator = '\n')
			.toByteSequence(Charset.defaultUnix)
	)

	val tmpModulesDirectory = directory.createFolder(".tmp.packages")

	val bashSession = LocalBashSession(directory)

	try {

		stores.forEach { store ->

			val storeDirectory = tmpModulesDirectory.createFolderIfNotExists(store.name)

			storeDirectory.empty()

			val packageNamesOfStore = packageNames.copyFilter { packageName ->
				getPackage(packageName).storeName == store.name
			}

			packageNamesOfStore.forEach { packageName ->

				val moduleDirectory = storeDirectory.createFolder(packageName)

				moduleDirectory.createSymbolicLink(
					name = "jar",
					linkPath = "../../../.ic/jar/$packageName"
				)

				moduleDirectory.createSymbolicLink(
					name = "Package.ll",
					linkPath = "../../../.ic/src/$packageName/Package.ll"
				)

				val mainDirectory = moduleDirectory.createFolder("main")

				mainDirectory.createSymbolicLinkIfExists(
					name = "jar",
					linkPath = "../../../../.ic/src/$packageName/jar"
				)

				mainDirectory.createSymbolicLink(
					name = "gen",
					linkPath = "../../../../.ic/gen/$packageName"
				)

				val pkgDecl = getPackageDeclaration(packageName)

				when (pkgDecl.type) {

					PackageDeclaration.Type.Normal -> setupModuleDefault(
						packageName = packageName,
						packageDeclaration = pkgDecl,
						moduleDirectory = moduleDirectory,
						mainDirectory = mainDirectory
					)

					PackageDeclaration.Type.Multiplatform -> setupModuleMultiplatform(
						packageName = packageName,
						packageDeclaration = pkgDecl,
						moduleDirectory = moduleDirectory,
						mainDirectory = mainDirectory
					)

				}

			}

		}

		bashSession.executeCommand("rm -rf packages; mv .tmp.packages packages")

	} catch (any: Throwable) {

		bashSession.executeCommand("rm -rf .tmp.packages")

		throw any

	}

}