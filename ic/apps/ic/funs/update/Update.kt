package ic.apps.ic.funs.update


import ic.apps.ic.dist.Distribution
import ic.apps.ic.ui.IcUi
import ic.apps.ic.funs.build.build
import ic.apps.ic.funs.update.project.updateProject
import ic.struct.set.finite.FiniteSet
import ic.struct.collection.ext.foreach.forEach


fun Distribution.update (

	packagesToUpdate : FiniteSet<String>,

	alreadyDownloadedPackages : FiniteSet<String> = FiniteSet(),

	icUi : IcUi

) {

	val session = UpdateSession(
		alreadyDownloadedPackages = alreadyDownloadedPackages
	)

	packagesToUpdate.forEach { packageName ->
		updatePackage(
			session = session,
			packageName = packageName,
			icUi = icUi
		)
	}

	if (toBuildOnUpdate) {
		build(
			packageNamesToBuild = session.updatedPackages,
			icUi = icUi
		)
	}

	updateProject()

}