package ic.apps.ic.funs.update


import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet


internal class UpdateSession (

	val alreadyDownloadedPackages : FiniteSet<String>

) {

	val updatedPackages = EditableSet<String>()

}