package ic.apps.ic.funs.versions


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.getPackageOrThrow
import ic.apps.ic.dist.ext.getPackageSrcRootDir
import ic.git.gitCheckout
import ic.base.throwables.NotExistsException
import ic.base.throwables.NotNeededException

import ic.apps.ic.ui.IcUi
import ic.apps.ic.funs.throwables.PackageNotExists


@Throws(PackageNotExists::class, NotExistsException::class, NotNeededException::class)
fun Distribution.switchVersion (

	packageName : String,

	version : String,

	icUi : IcUi

) {

	val packageInstall = try {
		getPackageOrThrow(packageName)
	} catch (notExists: NotExistsException) {
		throw PackageNotExists(packageName)
	}

	gitCheckout(
		repositoryDir 	= getPackageSrcRootDir(packageName),
		version 		= version,
		handleWarning 	= { message -> icUi.handleWarning(message) }
	)

	packageInstall.version = version

}