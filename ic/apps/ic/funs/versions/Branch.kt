package ic.apps.ic.funs.versions


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.getPackageOrThrow
import ic.apps.ic.dist.ext.getPackageSrcRootDir
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.git.gitCheckoutNewBranch
import ic.apps.ic.ui.IcUi
import ic.apps.ic.funs.throwables.PackageNotExists


@Throws(PackageNotExists::class, AlreadyExistsException::class)
fun Distribution.branch (

	packageName : String,

	branchName : String,

	icUi : IcUi

) {

	val pkg = try {
		getPackageOrThrow(packageName)
	} catch (notExists: NotExistsException) {
		throw PackageNotExists(packageName)
	}

	gitCheckoutNewBranch(
		repositoryDir 	= getPackageSrcRootDir(packageName),
		branchName 		= branchName,
		handleWarning 	= { message -> icUi.handleWarning(message) }
	)

	pkg.version = branchName

}