package ic.apps.ic.funs.add


import ic.base.throwables.*
import ic.util.auth.getAuth

import ic.git.gitClone

import ic.apps.ic.ui.IcUi
import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.Store
import ic.apps.ic.dist.ext.srcDirectory
import ic.storage.fs.Directory


@Throws(NotExistsException::class)
internal fun Distribution.implementClonePackage (

	packageName : String,

	store : Store,

	icUi : IcUi

) : Directory {

	try {
		return gitClone(
			baseDir = srcDirectory,
			url = store.url + "/" + packageName,
			auth = getAuth(store.url, "read") { icUi.askPullAuth(it) },
			destinationPath = packageName,
			handleWarning = { icUi.handleWarning(it) }
		)
	} catch (alreadyExists: AlreadyExistsException) {
		throw InconsistentDataException(alreadyExists)
	}

}