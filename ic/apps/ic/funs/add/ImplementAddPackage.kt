package ic.apps.ic.funs.add


import ic.apps.ic.dist.Distribution
import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.NotExistsException
import ic.struct.list.ext.foreach.forEach

import ic.git.getCurrentGitBranch

import ic.apps.ic.ui.IcUi
import ic.apps.ic.dist.Package
import ic.apps.ic.dist.ext.packageNames
import ic.apps.ic.dist.ext.save
import ic.apps.ic.dist.ext.stores


@Throws(AlreadyExistsException::class, NotExistsException::class)
internal fun Distribution.implementAddPackage (packageName: String, icUi: IcUi) {

	if (packageName in packageNames) throw AlreadyExistsException

	icUi.onPackageAddStarted(packageName)

	var pkg : Package? = null

	skippable {

		stores.forEach { store ->

			val clonedSrcDir = try {
				implementClonePackage(packageName, store, icUi)
			} catch (notExistsInStore: NotExistsException) { return@forEach }

			pkg = Package(
				name = packageName,
				storeName = store.name,
				version   = getCurrentGitBranch(clonedSrcDir)
			)

			skip

		}

	}

	packagesAsEditableList.add(pkg ?: throw NotExistsException)

	save()

}