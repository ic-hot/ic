package ic.apps.ic.funs.add


import ic.apps.ic.dist.Distribution
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.MessageException
import ic.base.throwables.NotExistsException
import ic.struct.set.finite.FiniteSet

import ic.apps.ic.ui.IcUi
import ic.apps.ic.funs.update.update


@Throws(
	AlreadyExistsException::class,
	NotExistsException::class,
	MessageException::class
)
fun Distribution.addPackage (

	packageName : String,

	icUi : IcUi

) {

	implementAddPackage(
		packageName = packageName,
		icUi 		= icUi
	)

	update(
		packagesToUpdate 			= FiniteSet(packageName),
		alreadyDownloadedPackages 	= FiniteSet(packageName),
		icUi 						= icUi
	)

}