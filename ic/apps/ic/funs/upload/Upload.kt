package ic.apps.ic.funs.upload


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.getPackageOrThrow
import ic.apps.ic.dist.ext.getPackageSrcRootDir
import ic.apps.ic.dist.ext.getStore
import ic.apps.ic.ui.IcUi
import ic.apps.ic.funs.throwables.PackageNotExists
import ic.git.RejectedDueToConflict
import ic.git.gitAddAll
import ic.git.gitCommit
import ic.git.gitPush
import ic.struct.collection.Collection
import ic.base.throwables.NotExistsException
import ic.base.throwables.NotNeededException
import ic.struct.collection.ext.foreach.forEach
import ic.util.auth.getAuth


@Throws(PackageNotExists::class, RejectedDueToConflict::class)
fun Distribution.upload (

	packageNames : Collection<String>,

	commitMessage : String,

	icUi : IcUi

) {

	packageNames.forEach { packageName ->

		val pkg = try {
			getPackageOrThrow(packageName)
		} catch (notExists: NotExistsException) {
			throw PackageNotExists(packageName)
		}

		icUi.onPackageUploadStarted(packageName)

		val packageSrcDirectory = getPackageSrcRootDir(packageName)

		gitAddAll(
			repositoryDir = packageSrcDirectory,
			handleWarning = { message -> icUi.handleWarning(message) }
		)

		try {
			gitCommit(
				repositoryDir = packageSrcDirectory,
				message = commitMessage
			)
		} catch (notNeeded: NotNeededException) {}

		val store = getStore(pkg.storeName)

		try {
			gitPush(
				repositoryDir = packageSrcDirectory,
				url = store.url + "/" + packageName,
				auth = getAuth(store.url, "write") { icUi.askPushAuth(it) },
				handleWarning = { message -> icUi.handleWarning(message) }
			)
		} catch (notNeeded: NotNeededException) {}

	}

}