package ic.apps.ic.funs.remove


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.*
import ic.apps.ic.dist.ext.binDirectory
import ic.apps.ic.dist.ext.genDirectory
import ic.apps.ic.dist.ext.jarDirectory
import ic.apps.ic.dist.ext.launchDirectory
import ic.base.throwables.NotExistsException
import ic.storage.fs.ext.removeIfExists
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.editable.ext.removeOneOrThrowNotExists


@Throws(NotExistsException::class)
fun Distribution.removePackageOrThrow (packageName: String) {

	packageNames.forEach {
		if (packageName in getPackageDeclaration(it).dependencies) {
			removePackage(it)
		}
	}

	launchDirectory	.removeIfExists(packageName)
	binDirectory	.removeIfExists(packageName)
	genDirectory	.removeIfExists(packageName)
	jarDirectory	.removeIfExists(packageName)
	srcDirectory	.removeIfExists(packageName)

	packagesAsEditableList.removeOneOrThrowNotExists { it.name == packageName }
	save()

}


fun Distribution.removePackage (packageName: String) {
	try {
		removePackageOrThrow(packageName)
	} catch (notExists: NotExistsException) {
		throw RuntimeException("packageName: $packageName")
	}
}