package ic.apps.ic.funs.throwables


class PackageNotExists (

	val packageName : String

) : Exception() {

	override fun fillInStackTrace() = null

}