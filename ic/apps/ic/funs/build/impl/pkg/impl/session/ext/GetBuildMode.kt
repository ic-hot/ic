package ic.apps.ic.funs.build.impl.pkg.impl.session.ext


import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession
import ic.apps.ic.funs.build.impl.util.BuildMode
import ic.apps.ic.funs.build.impl.util.getBuildMode


internal fun BuildPackageSession.getBuildMode() : BuildMode {

	return getBuildMode(
		packageSrcDirectories = packageSrcDirectories
	)

}