package ic.apps.ic.funs.build.impl.pkg.impl.jvm


import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.ext.getRelativePath
import ic.struct.collection.convert.ConvertCollection
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyJoin
import ic.struct.map.finite.FiniteMap
import ic.struct.collection.ext.copy.copyConvertToList
import ic.struct.list.ext.concat
import ic.struct.list.ext.copy.convert.copyConvert

import ic.apps.ic.dist.ext.getPackageJarDirectories
import ic.apps.ic.dist.ext.getPackageSrcDirs
import ic.apps.ic.dist.ext.launchDirectory
import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession

import ic.javac.generateJavaLaunchScript


internal fun BuildPackageSession.generateJvmLaunchScript () {

	generateJavaLaunchScript(
		distributionDirectory = distribution.directory,
		isPortable = distribution.isPortable,
		classDirs = ConvertCollection(fullDependenciesWithPackageName) { packageName ->
			binDirectory.createFolderIfNotExists(packageName)
		},
		jarDirs = (
			fullDependenciesWithPackageName
			.copyConvert { packageName ->
				distribution.getPackageJarDirectories(packageName)
			}
			.copyJoin()
		),
		mainClassName = "ic.app.Main",
		environmentVariables = FiniteMap(
			"IC_APP_PACKAGE_NAME" to packageName,
			"IC_SRC_DIRS" to (
				fullDependenciesWithPackageName
				.copyConvertToList { packageName ->
					distribution.getPackageSrcDirs(packageName)
				}
				.copyJoin()
				.copyConvert { srcDir ->
					distribution.directory.getRelativePath(of = srcDir)
				}
				.concat(':')
			)
		),
		targetBaseDirectory = distribution.launchDirectory,
		scriptName = packageName
	)

}