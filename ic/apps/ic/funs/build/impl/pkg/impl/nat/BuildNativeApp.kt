package ic.apps.ic.funs.build.impl.pkg.impl.nat


import ic.base.escape.skip.skip
import ic.base.throwables.CompilationException
import ic.base.throwables.MessageException
import ic.cc.compileCFile
import ic.storage.fs.FsLocation
import ic.storage.fs.ext.recursive.recursiveGetAllRelativePaths
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyJoin
import ic.struct.list.ext.copy.convert.copyConvert

import ic.apps.ic.dist.ext.getPackageSoDirectories
import ic.apps.ic.dist.ext.launchDirectory
import ic.apps.ic.funs.build.impl.pkg.impl.nat.impl.mergeDependenciesIntoTmpSrc
import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession


@Throws(MessageException::class)
internal fun BuildPackageSession.buildCApp() {

	mergeDependenciesIntoTmpSrc()

	val libsFiles = fullDependenciesWithPackageName.copyConvert { dependency ->
		distribution
		.getPackageSoDirectories(dependency)
		.copyConvert { soDirectory ->
			soDirectory.recursiveGetAllRelativePaths(toIncludeFolders = false).copyConvert { path ->
				if (!path.endsWith(".so")) skip
				FsLocation(soDirectory, path)
			}
		}
		.copyJoin()
	}.copyJoin()

	try {

		compileCFile(
			inputFile = FsLocation(
				baseDirectory = packageTmpSrcFolder,
				relativePath = "ic/app/main.c"
			),
			libsFiles = libsFiles,
			outputFile = FsLocation(
				baseDirectory = distribution.launchDirectory,
				relativePath = packageName
			),
			handleWarnings = icUi::handleWarning
		)

	} catch (t: CompilationException) {
		throw MessageException(t.message ?: "CompilationException")
	}

}