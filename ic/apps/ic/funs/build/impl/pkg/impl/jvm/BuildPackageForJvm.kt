package ic.apps.ic.funs.build.impl.pkg.impl.jvm


import ic.base.throwables.CompilationException
import ic.base.throwables.MessageException
import ic.base.throwables.NotNeededException
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyJoin

import ic.javac.compileJava

import ic.kotlinc.compileKotlin

import ic.apps.ic.javaVersion
import ic.apps.ic.dist.ext.getPackageJarDirectories
import ic.apps.ic.funs.build.impl.pkg.impl.session.ext.getBinDirectory
import ic.apps.ic.funs.build.impl.pkg.impl.jvm.impl.mergeGenAndSrcIntoTmpSrc
import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession


@Throws(MessageException::class)
internal fun BuildPackageSession.buildPackageForJvm() {

	mergeGenAndSrcIntoTmpSrc()

	val compiledDependencyDirs = fullDependenciesWithPackageName.copyConvert {
		getBinDirectory(it)
	}

	val jarDependenciesDirs = (
		fullDependenciesWithPackageName.copyConvert { packageName ->
			distribution.getPackageJarDirectories(packageName)
		}.copyJoin()
	)

	try {

		try {
			compileKotlin(
				inputDir = packageTmpSrcFolder,
				outputDir = packageTmpBinDirectory,
				compiledDependencyDirs = compiledDependencyDirs,
				jarDependenciesDirs = jarDependenciesDirs,
				javaVersion = javaVersion,
				handleWarning = { icUi.handleWarning(it) }
			)
		} catch (ignored: NotNeededException) {}

		try {
			compileJava(
				inputDir = packageTmpSrcFolder,
				outputDir = packageTmpBinDirectory,
				compiledDependencyDirs = compiledDependencyDirs,
				jarDependencyDirs = jarDependenciesDirs,
				handleWarning = { icUi.handleWarning(it) },
				javaVersion = javaVersion
			)
		} catch (ignored: NotNeededException) {}

	} catch (compilationError: CompilationException) {
		throw Exception(compilationError)
	}

}