package ic.apps.ic.funs.build.impl.pkg.impl.nat.impl


import ic.system.funs.executeBashCommand

import ic.apps.ic.dist.ext.getPackageGenDirectory
import ic.apps.ic.dist.ext.getPackageSrcDirs
import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession
import ic.struct.collection.ext.foreach.forEach
import ic.struct.list.ext.foreach.forEach


internal fun BuildPackageSession.mergeDependenciesIntoTmpSrc() {

	fullDependenciesWithPackageName.forEach { packageName ->

		val genDirectory = distribution.getPackageGenDirectory(packageName)

		executeBashCommand(
			"cp -rf " +
			"${ genDirectory.absolutePath }/* " +
			"${ packageTmpSrcFolder.absolutePath }/"
		)

		distribution.getPackageSrcDirs(packageName).forEach { srcDirectory ->

			executeBashCommand(
				"cp -rf " +
				"${ srcDirectory.absolutePath }/* " +
				"${ packageTmpSrcFolder.absolutePath }/"
			)

		}

	}

}