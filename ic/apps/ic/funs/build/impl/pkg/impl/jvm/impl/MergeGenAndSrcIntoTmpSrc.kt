package ic.apps.ic.funs.build.impl.pkg.impl.jvm.impl


import ic.system.funs.executeBashCommand

import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession
import ic.struct.list.ext.foreach.forEach


internal fun BuildPackageSession.mergeGenAndSrcIntoTmpSrc() {

	executeBashCommand(
		"cp -rf " +
		"${ packageGenDirectory.absolutePath }/* " +
		"${ packageTmpSrcFolder.absolutePath }/"
	)

	packageSrcDirectories.forEach { packageSrcDirectory ->
		executeBashCommand(
			"cp -rf " +
			"${ packageSrcDirectory.absolutePath }/* " +
			"${ packageTmpSrcFolder.absolutePath }/"
		)
	}

}