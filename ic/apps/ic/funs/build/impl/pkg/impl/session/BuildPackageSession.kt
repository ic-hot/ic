package ic.apps.ic.funs.build.impl.pkg.impl.session


import ic.storage.fs.ext.createFolder
import ic.struct.collection.ext.plus

import ic.apps.ic.dist.ext.getFullDependencies
import ic.apps.ic.dist.ext.getPackageDeclaration
import ic.apps.ic.dist.ext.getPackageSrcDirs
import ic.apps.ic.dist.ext.recreatePackageGenDirectory
import ic.apps.ic.funs.build.impl.pkg.impl.session.ext.getBuildMode
import ic.apps.ic.funs.build.impl.session.BuildSession


internal class BuildPackageSession (

	val buildSession : BuildSession,

	val packageName : String

) {


	val icUi get() = buildSession.icUi

	val distribution get() = buildSession.distribution

	val binDirectory get() = buildSession.binDirectory


	val packageDecl = distribution.getPackageDeclaration(packageName)

	val fullDependencies = distribution.getFullDependencies(packageName)

	val fullDependenciesWithPackageName = fullDependencies + packageName


	val packageSrcDirectories = distribution.getPackageSrcDirs(packageName)

	val packageGenDirectory = distribution.recreatePackageGenDirectory(packageName)

	val packageTmpSrcFolder = buildSession.tmpSrcDirectory.createFolder(packageName)

	val packageTmpBinDirectory = buildSession.tmpBinDirectory.createFolder(packageName)


	val buildMode = getBuildMode()


}