package ic.apps.ic.funs.build.impl.pkg.impl.session.ext


import ic.storage.fs.Directory
import ic.storage.fs.ext.getFolder

import ic.apps.ic.dist.ext.binDirectory
import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession


internal fun BuildPackageSession.getBinDirectory (packageName: String) : Directory {

	return when (packageName) {

		this.packageName -> packageTmpBinDirectory

		in buildSession.packageNamesToBuild -> {

			buildSession.tmpBinDirectory.getFolder(packageName)

		}

		else -> {
			distribution.binDirectory.getFolder(packageName)
		}

	}

}