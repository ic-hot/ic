package ic.apps.ic.funs.build.impl.pkg


import ic.base.throwables.MessageException
import ic.struct.collection.ext.foreach.forEach

import ic.apps.ic.funs.build.impl.session.BuildSession
import ic.apps.ic.funs.build.impl.pkg.impl.jvm.buildPackageForJvm
import ic.apps.ic.funs.build.impl.pkg.impl.jvm.generateJvmLaunchScript
import ic.apps.ic.funs.build.impl.pkg.impl.nat.buildCApp
import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession
import ic.apps.ic.funs.build.impl.test.testPackage
import ic.apps.ic.funs.build.impl.util.BuildMode


@Throws(MessageException::class)
internal fun BuildSession.buildPackageIfNotAlready (packageName: String) {

	if (packageName in alreadyBuiltPackageNames) return

	BuildPackageSession(
		buildSession = this,
		packageName = packageName
	).apply {

		packageDecl.dependencies.forEach { dependencyPackageName ->
			if (dependencyPackageName in packageNamesToBuild) {
				buildPackageIfNotAlready(dependencyPackageName)
			}
		}

		icUi.notifyPackageBuildStarted(packageName)

		when (buildMode) {

			BuildMode.Library -> buildPackageForJvm()

			is BuildMode.App.C -> buildCApp()

			is BuildMode.App.Ll -> throw NotImplementedError()

			BuildMode.App.Jvm -> {
				buildPackageForJvm()
				generateJvmLaunchScript()
			}

		}

		testPackage()

	}

	alreadyBuiltPackageNames.add(packageName)

}