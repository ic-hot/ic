package ic.apps.ic.funs.build.impl.test


import ic.apps.ic.funs.build.impl.pkg.impl.session.BuildPackageSession


internal fun BuildPackageSession.testPackage() {

	// TODO

	/*val testEntryPoint = getPackage(packageName).test

	if (testEntryPoint == null) return

	val allRequiredPackages = UnionFiniteSet(
		FiniteSet(
			getFullDependencies(packageName)
		),
		FiniteSet(packageName)
	)
	generateJavaLaunchScript(
		distributionDirectory = rootDirectory,
		isPortable = false,
		classDirs = ConvertCollection(allRequiredPackages) { dependency ->
			if (buildSession.packageNamesToBuild.contains(dependency)) {
				buildSession.tmpBinDirectory.get(dependency)
			} else {
				buildSession.binDirectory.get(dependency)
			}
		},
		jarDirs = (
			allRequiredPackages
				.copyConvert { dependency -> getPackageJarDirectories(dependency) }
				.copyJoin()
			),
		mainClassName = testEntryPoint,
		environmentVariables = FiniteMap(
			"IC_APP_PACKAGE_NAME" to packageName,
			"IC_LAUNCH_MODE" to "test"
		),
		args = Text.Empty,
		targetBaseDirectory = buildSession.tmpLaunchTestDirectory,
		scriptName = packageName
	)
	run {
		icUi.onPackageTestStarted(packageName)
		val testResult = LocalBashSession(rootDirectory).executeCommand(
			buildSession.tmpLaunchTestDirectory.absolutePath + "/" + packageName
		)
		if (testResult.endsWith("All tests passed.")) {
			icUi.onPackageTestPassed(packageName, testResult)
		} else {
			icUi.onPackageTestFailed(packageName, testResult)
			throw FatalException("Test failed for package $packageName")
		}
	}

	val tmpPackageDir = buildSession.tmpBinDirectory.get<Directory>(packageName)
	binDirectory.set(packageName, tmpPackageDir)*/

}