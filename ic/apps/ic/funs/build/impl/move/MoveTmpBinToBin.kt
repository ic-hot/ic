package ic.apps.ic.funs.build.impl.move


import ic.storage.fs.ext.getItem
import ic.struct.collection.ext.foreach.forEach

import ic.apps.ic.funs.build.impl.session.BuildSession


internal fun BuildSession.moveTmpBinToBin() {

	packageNamesToBuild.forEach { packageName ->

		binDirectory.moveHereReplacing(packageName, tmpBinDirectory.getItem(packageName))

	}

}