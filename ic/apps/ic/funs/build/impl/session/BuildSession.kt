package ic.apps.ic.funs.build.impl.session


import ic.apps.ic.dist.Distribution
import ic.storage.fs.Directory
import ic.storage.fs.ext.createFolder
import ic.struct.set.finite.FiniteSet
import ic.struct.set.editable.EditableSet

import ic.apps.ic.dist.ext.binDirectory
import ic.apps.ic.ui.IcUi


internal class BuildSession (

	val distribution : Distribution,

	val packageNamesToBuild : FiniteSet<String>,

	val tmpDirectory : Directory,

	val icUi : IcUi

) {

	val binDirectory = distribution.binDirectory

	val tmpSrcDirectory = tmpDirectory.createFolder("src")

	val tmpBinDirectory = tmpDirectory.createFolder("bin")

	val alreadyBuiltPackageNames = EditableSet<String>()

}