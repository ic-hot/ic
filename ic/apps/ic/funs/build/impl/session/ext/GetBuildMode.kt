package ic.apps.ic.funs.build.impl.session.ext


import ic.apps.ic.dist.ext.getPackageSrcDirs
import ic.apps.ic.funs.build.impl.session.BuildSession
import ic.apps.ic.funs.build.impl.util.getBuildMode


internal fun BuildSession.getBuildMode (packageName: String) = getBuildMode(
	packageSrcDirectories = distribution.getPackageSrcDirs(packageName)
)