package ic.apps.ic.funs.build.impl.util


internal sealed class BuildMode {


	object Library : BuildMode()


	sealed class App : BuildMode() {

		object Jvm : App()

		object C : App()

		object Ll : App()

	}


}