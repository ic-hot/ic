package ic.apps.ic.funs.build.impl.util


import ic.base.throwables.MessageException
import ic.storage.fs.Directory
import ic.storage.fs.ext.recursive.recursiveGetAllRelativePaths
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyJoin
import ic.struct.list.ext.copy.copyToFiniteSet


@Throws(MessageException::class)
internal fun getBuildMode (

	packageSrcDirectories : Collection<Directory>

) : BuildMode {

	val allSrcFilesPaths = packageSrcDirectories.copyConvert { srcDirectory ->

		srcDirectory.recursiveGetAllRelativePaths(toIncludeFolders = false)

	}.copyJoin().copyToFiniteSet()

	when {

		allSrcFilesPaths contains "ic/app/Main.kt" ||
		allSrcFilesPaths contains "ic/app/Main.kt" -> return BuildMode.App.Jvm

		allSrcFilesPaths contains "ic/app/main.c" -> return BuildMode.App.C

		allSrcFilesPaths contains "ic/app/Main.ll" -> return BuildMode.App.Ll

		else -> return BuildMode.Library

	}

}