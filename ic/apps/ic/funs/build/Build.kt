package ic.apps.ic.funs.build


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ext.icDirectory
import ic.base.throwables.MessageException
import ic.struct.set.finite.FiniteSet

import ic.apps.ic.ui.IcUi
import ic.apps.ic.dist.ext.tmpDirectory
import ic.apps.ic.funs.build.impl.move.moveTmpBinToBin
import ic.apps.ic.funs.build.impl.pkg.buildPackageIfNotAlready
import ic.apps.ic.funs.build.impl.session.BuildSession
import ic.apps.ic.dist.ext.packageNames

import ic.storage.fs.ext.removeIfExists
import ic.struct.collection.ext.foreach.nonBreakableForEach


@Throws(MessageException::class)
fun Distribution.build (

	packageNamesToBuild : FiniteSet<String> = packageNames,

	icUi : IcUi

) {

	icDirectory.removeIfExists("tmp")

	val tmpDirectory = this.tmpDirectory

	BuildSession(
		distribution = this,
		packageNamesToBuild = packageNamesToBuild,
		tmpDirectory = tmpDirectory,
		icUi = icUi
	).apply {

		packageNamesToBuild.nonBreakableForEach {
			buildPackageIfNotAlready(it)
		}

		moveTmpBinToBin()

		tmpDirectory.remove()

	}

}