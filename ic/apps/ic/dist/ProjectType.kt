package ic.apps.ic.dist


sealed class ProjectType {

	data object Default : ProjectType()

	data object Gradle : ProjectType()

}