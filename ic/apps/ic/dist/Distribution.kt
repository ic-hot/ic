package ic.apps.ic.dist


import ic.storage.fs.Directory
import ic.struct.list.List
import ic.struct.list.ext.copy.copyToEditableListIfNotAlready


class Distribution (

	val directory : Directory,

	var launchPath : String,

	var isPortable : Boolean,

	var projectType : ProjectType?,

	var toBuildOnUpdate : Boolean,

	stores : List<Store>,

	packages : List<Package>

) {

	internal val storesAsEditableList = stores.copyToEditableListIfNotAlready()

	internal val packagesAsEditableList = packages.copyToEditableListIfNotAlready()

	companion object

}