package ic.apps.ic.dist


import ic.struct.list.List
import ic.struct.set.finite.FiniteSet


data class PackageDeclaration (

	val type : Type,

	val dependencies : FiniteSet<String>,

	val jarDependencies : FiniteSet<String>,

	val stores : List<Store>

) {

	sealed class Type {
		object Normal        : Type()
		object Multiplatform : Type()
	}

}