package ic.apps.ic.dist.ext


import ic.apps.ic.dist.Distribution
import ic.storage.fs.Directory
import ic.storage.fs.local.ext.createIfNotExists
import ic.struct.value.cached.Cached
import ic.struct.value.ext.getValue


val Distribution.Companion.global by Cached {

	val dir = Directory.createIfNotExists("/ic")

	Distribution.loadOrNull(dir) ?:

	Distribution.create(
		directory = dir,
		isPortable 		= false,
		launchPath 		= "/usr/bin",
		toBuildOnUpdate = true,
		projectType		= null
	)

}