package ic.apps.ic.dist.ext


import ic.struct.collection.Collection
import ic.storage.fs.Directory
import ic.storage.fs.ext.*
import ic.storage.fs.local.ext.createIfNotExists
import ic.base.throwables.NotExistsException
import ic.struct.collection.ext.plus
import ic.struct.list.List

import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.PackageDeclaration
import ic.struct.list.ext.copy.convert.copyConvert


internal val Distribution.icDirectory get() = directory.createFolderIfNotExists(".ic")

internal val Distribution.srcDirectory get() = icDirectory.createFolderIfNotExists("src")
internal val Distribution.binDirectory get() = icDirectory.createFolderIfNotExists("bin")
internal val Distribution.genDirectory get() = icDirectory.createFolderIfNotExists("gen")
internal val Distribution.jarDirectory get() = icDirectory.createFolderIfNotExists("jar")
internal val Distribution.soDirectory  get() = icDirectory.createFolderIfNotExists("so")


@Throws(NotExistsException::class)
internal fun Distribution.getPackageSrcRootDirOrThrow (packageName: String) : Directory {
	return srcDirectory.getFolderOrThrow(packageName)
}

internal fun Distribution.getPackageSrcRootDir (packageName: String) : Directory {
	try {
		return getPackageSrcRootDirOrThrow(packageName)
	} catch (_: NotExistsException) {
		throw RuntimeException("packageName: $packageName")
	}
}

val multiplatformDirsNames = List("common", "jvm", "jvm-nonandroid", "ic")

internal fun Distribution.getPackageSrcDirs (packageName: String) : List<Directory> {
	val type = getPackageDeclaration(packageName).type
	val pkgSrcRootDir = getPackageSrcRootDir(packageName)
	return when (type) {
		PackageDeclaration.Type.Normal -> List(pkgSrcRootDir)
		PackageDeclaration.Type.Multiplatform -> multiplatformDirsNames.copyConvert {
			pkgSrcRootDir.getFolderOrSkip(it)
		}
	}
}


internal fun Distribution.getPackageJarDirectory (packageName: String) : Directory {
	return jarDirectory.createFolderIfNotExists(packageName)
}

internal fun Distribution.getPackageJarDirectories (
	packageName : String
) : Collection<Directory> {
	return (
		Collection(
			getPackageJarDirectory(packageName)
		) +
		getPackageSrcDirs(packageName).copyConvert { it.getFolderOrSkip("jar") }
	)
}


internal fun Distribution.recreatePackageGenDirectory (packageName: String) : Directory {
	val genDirectory = this.genDirectory
	genDirectory.removeIfExists(packageName)
	return genDirectory.createFolder(packageName)
}

internal fun Distribution.getPackageGenDirectory (packageName: String) : Directory {
	return genDirectory.createFolderIfNotExists(packageName)
}


internal fun Distribution.getPackageSoDirectory (packageName: String) : Directory {
	return soDirectory.createFolderIfNotExists(packageName)
}

internal fun Distribution.getPackageSoDirectories (
	packageName : String
) : Collection<Directory> {
	return (
		Collection(
			getPackageSoDirectory(packageName)
		) +
			getPackageSrcDirs(packageName).copyConvert { packageSrcDirectory ->
				packageSrcDirectory.getFolderOrSkip("so-x64")
			}
		)
}


val Distribution.tmpDirectory get() = icDirectory.createFolderIfNotExists("tmp")

internal val Distribution.launchDirectory get() = Directory.createIfNotExists(directory, launchPath)