package ic.apps.ic.dist.ext


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.PackageDeclaration
import ic.apps.ic.dist.Store
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.copy.convert.copyConvertToFiniteSet

import ic.storage.fs.ext.asLlStorage
import ll.lang.ext.*


fun Distribution.getPackageDeclaration (packageName: String) : PackageDeclaration {

	val obj = getPackageSrcRootDir(packageName).asLlStorage["Package"]

	return PackageDeclaration(
		type = when (val str = obj["Type"].asStringOrNull) {
			null            -> PackageDeclaration.Type.Normal
			"Normal"        -> PackageDeclaration.Type.Normal
			"Multiplatform" -> PackageDeclaration.Type.Multiplatform
			else -> throw RuntimeException("Type: $str")
		},
		dependencies = obj["Dependencies"].asListOrEmpty.copyConvertToFiniteSet { it.asString },
		jarDependencies = obj["JarDependencies"].asListOrEmpty.copyConvertToFiniteSet { it.asString },
		stores = obj["Stores"].asListOrEmpty.copyConvert { storeObj ->
			Store(
				name = storeObj["Name"].asString,
				url = storeObj["Url"].asString
			)
		}
	)

}