package ic.apps.ic.dist.ext


import ic.apps.ic.dist.Distribution
import ic.base.throwables.MessageException
import ic.storage.fs.local.workdir
import ic.struct.value.cached.Cached
import ic.struct.value.ext.getValue


val Distribution.Companion.local by Cached {

	Distribution.loadOrNull(workdir) ?:

	throw MessageException(
		workdir.absolutePath + " is not a valid InstantCoffee distribution"
	)

}