package ic.apps.ic.dist.ext


import ic.struct.list.List

import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.Package


val Distribution.packages : List<Package> get() = packagesAsEditableList