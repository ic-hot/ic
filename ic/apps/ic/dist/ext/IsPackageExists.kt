package ic.apps.ic.dist.ext


import ic.struct.list.ext.reduce.find.atLeastOne

import ic.apps.ic.dist.Distribution


fun Distribution.isPackageExists (name: String) = (

	packagesAsEditableList.atLeastOne { it.name == name }

)