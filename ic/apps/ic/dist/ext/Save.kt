package ic.apps.ic.dist.ext


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.ProjectType
import ic.storage.fs.ext.asLlStorage
import ic.struct.list.ext.copy.convert.copyConvert

import ll.lang.Object


fun Distribution.save() {

	directory.asLlStorage["Distribution"] = Object(
		"LaunchPath" to launchPath,
		"Stores" to stores.copyConvert { store ->
			Object(
				"Name" to store.name,
				"Url"  to store.url
			)
		},
		"Packages" to packages.copyConvert { pkg ->
			Object(
				"Name"      to pkg.name,
				"StoreName" to pkg.storeName,
				"Version"   to pkg.version
			)
		},
		"IsPortable" to isPortable,
		"ProjectType" to when (projectType) {
			null                -> null
			ProjectType.Default -> "Default"
			ProjectType.Gradle  -> "Gradle"
		},
		"ToBuildOnUpdate" to toBuildOnUpdate
	)

}