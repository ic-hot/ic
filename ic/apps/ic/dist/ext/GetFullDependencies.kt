package ic.apps.ic.dist.ext


import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyUnion
import ic.struct.set.finite.FiniteSet
import ic.struct.set.finite.ext.plus

import ic.apps.ic.dist.Distribution


fun Distribution.getFullDependencies (packageName: String) : FiniteSet<String> {

	val dependencies = getPackageDeclaration(packageName).dependencies

	return dependencies + dependencies.copyConvert { getFullDependencies(it) }.copyUnion()

}