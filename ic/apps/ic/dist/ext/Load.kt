package ic.apps.ic.dist.ext


import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.Package
import ic.apps.ic.dist.ProjectType
import ic.apps.ic.dist.Store
import ic.base.throwables.MessageException
import ic.storage.fs.Directory
import ic.storage.fs.ext.asLlStorage
import ic.struct.list.ext.copy.convert.copyConvert

import ll.lang.ext.*


fun Distribution.Companion.loadOrNull (directory: Directory) : Distribution? {

	val obj = directory.asLlStorage["Distribution"] ?: return null

	return Distribution(
		directory = directory,
		launchPath = obj["LaunchPath"].asString,
		isPortable = obj["IsPortable"].asBoolean,
		projectType = when (val string = obj["ProjectType"].asStringOrNull) {
			null -> null
			"Default" -> ProjectType.Default
			"Gradle" -> ProjectType.Gradle
			else -> throw RuntimeException("projectType: $string")
		},
		toBuildOnUpdate = obj["ToBuildOnUpdate"].asBoolean,
		stores = obj["Stores"].asList.copyConvert { storeObj ->
			Store(
				name = storeObj["Name"].asString,
				url  = storeObj["Url"].asString
			)
		},
		packages = obj["Packages"].asList.copyConvert { packageObj ->
			Package(
				name = packageObj["Name"].asString,
				storeName = packageObj["StoreName"].asString,
				version = packageObj["Version"].asString
			)
		}
	)

}