package ic.apps.ic.dist.ext


import ic.storage.fs.Directory
import ic.struct.list.List

import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.Package
import ic.apps.ic.dist.ProjectType
import ic.apps.ic.dist.Store


fun Distribution.Companion.create (

	directory : Directory,

	launchPath : String,

	isPortable : Boolean,

	projectType : ProjectType?,

	toBuildOnUpdate : Boolean,

	stores : List<Store> = List(
		Store(
			name = "ic",
			url = "https://gitlab.com/ic-hot",
		)
	),

	packages : List<Package> = List()

) : Distribution {

	val distribution = Distribution(
		directory = directory,
		launchPath = launchPath,
		isPortable = isPortable,
		projectType = projectType,
		toBuildOnUpdate = toBuildOnUpdate,
		stores = stores,
		packages = packages
	)

	distribution.save()

	return distribution

}


fun Distribution.Companion.createPortable (
	directory : Directory,
	projectType : ProjectType? = null
) : Distribution {
	return Distribution.create(
		directory = directory,
		isPortable 		= true,
		launchPath 		= "launch",
		toBuildOnUpdate = false,
		projectType		= projectType
	)
}