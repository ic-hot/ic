package ic.apps.ic.dist.ext


import ic.struct.list.ext.copy.convert.copyConvertToFiniteSet

import ic.apps.ic.dist.Distribution


val Distribution.packageNames get() = packages.copyConvertToFiniteSet { it.name }