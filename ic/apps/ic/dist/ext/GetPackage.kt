package ic.apps.ic.dist.ext


import ic.base.throwables.NotExistsException
import ic.struct.list.ext.reduce.find.find
import ic.struct.list.ext.reduce.find.findOrThrow

import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.Package


@Throws(NotExistsException::class)
fun Distribution.getPackageOrThrow (name: String) : Package {
	return packagesAsEditableList.findOrThrow { it.name == name }
}

fun Distribution.getPackage (name: String) : Package {
	try {
		return packagesAsEditableList.find { it.name == name }
	} catch (_: NotExistsException) {
		throw RuntimeException("name: $name")
	}
}