package ic.apps.ic.dist.ext


import ic.base.throwables.AlreadyExistsException
import ic.struct.list.List
import ic.struct.list.ext.reduce.find.atLeastOne
import ic.apps.ic.dist.Distribution
import ic.apps.ic.dist.Store
import ic.apps.ic.funs.remove.removePackage
import ic.base.throwables.NotExistsException
import ic.struct.list.editable.ext.removeOneOrThrowNotExists
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.reduce.find.findOrThrow


val Distribution.stores : List<Store> get() = storesAsEditableList


@Throws(NotExistsException::class)
fun Distribution.getStoreOrThrow (name: String) : Store {
	return storesAsEditableList.findOrThrow { it.name == name }
}

fun Distribution.getStore (name: String) : Store {
	try {
		return getStoreOrThrow(name)
	} catch (_: NotExistsException) {
		throw RuntimeException("name: $name")
	}
}


fun Distribution.isStoreExists (name: String) = storesAsEditableList.atLeastOne { it.name == name }


@Throws(AlreadyExistsException::class)
fun Distribution.addStoreOrThrow (name: String, url: String) {
	if (isStoreExists(name)) throw AlreadyExistsException
	storesAsEditableList.add(
		Store(
			name = name,
			url = url
		)
	)
}

@Throws(NotExistsException::class)
fun Distribution.removeStoreOrThrow (storeName: String) {
	packages.forEach { pkg ->
		if (pkg.storeName == storeName) {
			removePackage(pkg.name)
		}
	}
	storesAsEditableList.removeOneOrThrowNotExists { it.name == storeName }
}