Install:

First install jdk and kotlin.

Then:

    git clone https://gitlab.com/ic-hot/ic-install
    cd ic-install
    sudo bash install-ic.sh
    cd ..
    rm -rf ic-install